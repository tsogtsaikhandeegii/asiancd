import Link from 'next/link'
import { Locale } from '@/i18n.config'
import { getDictionary } from '@/lib/dictionary'
import LocaleSwitcher from './locale-switcher'
import Image from 'next/image'
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";


export default async function Header({ lang }: { lang: Locale }) {
  const { navigation } = await getDictionary(lang)

  return (
    <div className='isolate flex items-center bg-white'>
      <div className='absolute left-10'>

        <Link href={`/${lang}`}>
          <img className='' src="https://gitlab.com/tsogtsaikhandeegii/asiancd/-/blob/master/public/353599738_951156176282305_1139438072319551547_n.jpg?ref_type=heads" alt='logo' width={120} height={120} />
          
        </Link>
      </div>
      <div className="flex w-full lg:justify-center lg:items-center border-b  py-6 lg:border-none">
        <nav aria-label="Global">
          <div className="hidden lg:flex lg:gap-x-12">
          <ul className='flex gap-x-8'>
            <li>
              <Link href={`/${lang}`}>{navigation.home}</Link>
            </li>
            <li>
              <Link href={`/${lang}/about`}>{navigation.about}</Link>
            </li>
          </ul>
          </div>
          
        </nav>
</div>
      <header className='py-6'>
        <nav className='container flex items-center justify-between'>
          {/* <ul className='flex gap-x-8'>
            <li>
              <Link href={`/${lang}`}>{navigation.home}</Link>
            </li>
            <li>
              <Link href={`/${lang}/about`}>{navigation.about}</Link>
            </li>
          </ul> */}
          <LocaleSwitcher />
        </nav>
      </header>
    </div>
  )
}
