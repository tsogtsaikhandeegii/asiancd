import Link from "next/link";
import Image from "next/image";

export default function PromoSectionHome() {
//   return (
//     <section
//   className="relative bg-[url(https://scontent.fuln6-1.fna.fbcdn.net/v/t39.30808-6/353599738_951156176282305_1139438072319551547_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=5f2048&_nc_ohc=r3saJNsYehkAX8-W8wV&_nc_ht=scontent.fuln6-1.fna&cb_e2o_trans=q&oh=00_AfAkzalphDXiuexLvX9jrCV_HVm0w6eI87qcFSdlK96K-Q&oe=653A06D1)] bg-cover bg-center bg-no-repeat"
// >
//   <div
//     className="absolute inset-0 bg-white/75 sm:bg-transparent sm:from-white/95 sm:to-white/25 ltr:sm:bg-gradient-to-r rtl:sm:bg-gradient-to-l"
//   ></div>

//   <div
//     className="relative mx-auto max-w-screen-xl px-4 py-32 sm:px-6 lg:flex lg:h-screen lg:items-center lg:px-8"
//   >
//     <div className="max-w-xl text-center ltr:sm:text-left rtl:sm:text-right">
//       <h1 className="text-3xl font-extrabold sm:text-5xl">
//         Let us find your

//         <strong className="block font-extrabold text-rose-700">
//           Forever Home.
//         </strong>
//       </h1>

//       <p className="mt-4 max-w-lg sm:text-xl/relaxed">
//         Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nesciunt illo
//         tenetur fuga ducimus numquam ea!
//       </p>

//       <div className="mt-8 flex flex-wrap gap-4 text-center">
//         <a
//           href="#"
//           className="block w-full rounded bg-rose-600 px-12 py-3 text-sm font-medium text-white shadow hover:bg-rose-700 focus:outline-none focus:ring active:bg-rose-500 sm:w-auto"
//         >
//           Get Started
//         </a>

//         <a
//           href="#"
//           className="block w-full rounded bg-white px-12 py-3 text-sm font-medium text-rose-600 shadow hover:text-rose-700 focus:outline-none focus:ring active:text-rose-500 sm:w-auto"
//         >
//           Learn More
//         </a>
//       </div>
//     </div>
//   </div>
// </section>

//   );

  return (
    <div className="relative">
      <img
        src="https://scontent.fuln6-1.fna.fbcdn.net/v/t39.30808-6/353599738_951156176282305_1139438072319551547_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=5f2048&_nc_ohc=r3saJNsYehkAX8-W8wV&_nc_ht=scontent.fuln6-1.fna&cb_e2o_trans=q&oh=00_AfAkzalphDXiuexLvX9jrCV_HVm0w6eI87qcFSdlK96K-Q&oe=653A06D1"
        className="absolute inset-0 object-cover w-full h-full"
        alt=""
      />
      <div className="relative bg-gray-900 bg-opacity-75">
        <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
          <div className="flex flex-col items-center justify-between xl:flex-row">
            <div className="w-full max-w-xl mb-12 xl:mb-0 xl:pr-16 xl:w-7/12">
              <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-white sm:text-4xl sm:leading-none">
                The quick, brown fox <br className="hidden md:block" />
                jumps over a{' '}
                <span className="text-teal-accent-400">lazy dog</span>
              </h2>
              <p className="max-w-xl mb-4 text-base text-gray-400 md:text-lg">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudan, totam rem aperiam, eaque ipsa
                quae.
              </p>
              <a
                href="/"
                aria-label=""
                className="inline-flex items-center font-semibold tracking-wider transition-colors duration-200 text-teal-accent-400 hover:text-teal-accent-700"
              >
                Learn more
                <svg
                  className="inline-block w-3 ml-2"
                  fill="currentColor"
                  viewBox="0 0 12 12"
                >
                  <path d="M9.707,5.293l-5-5A1,1,0,0,0,3.293,1.707L7.586,6,3.293,10.293a1,1,0,1,0,1.414,1.414l5-5A1,1,0,0,0,9.707,5.293Z" />
                </svg>
              </a>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  );
}
